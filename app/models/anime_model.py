import psycopg2
from psycopg2 import sql
from . import conn_cur, commit_and_close, create_table


class Anime():
    anime_keys = ['Id', 'Anime', 'Released_date', 'Seasons']

    def __init__(self, anime: str, released_date: str, seasons: int):
        self.anime = anime.title()
        self.released_date = released_date
        self.seasons = seasons

    @staticmethod
    def all_animes():
        conn, cur = conn_cur()

        cur.execute(
            'SELECT * FROM animes;'
        )

        animes_fetched = cur.fetchall()

        commit_and_close(conn, cur)

        animes_list = [dict(zip(Anime.anime_keys, anime))
                       for anime in animes_fetched]

        return animes_list

    def create_anime(self):
        conn, cur = conn_cur()

        query = """
            INSERT INTO animes (anime, released_date, seasons) 
            VALUES
                (%s, %s, %s) RETURNING *;
            """

        query_values = list(self.__dict__.values())

        cur.execute(query, query_values)

        inserted_data = cur.fetchone()
        commit_and_close(conn, cur)

        return dict(zip(self.anime_keys, inserted_data))

    @staticmethod
    def get_by_id(id):
        conn, cur = conn_cur()

        cur.execute("""
            SELECT * FROM animes  
            WHERE id = (%s); 
            """, (id,),)

        returning_data = cur.fetchone()

        anime = dict(zip(Anime.anime_keys, returning_data))

        commit_and_close(conn, cur)

        return anime

    @staticmethod
    def update_anime_by_id(anime_id, data):
        conn, cur = conn_cur()

        columns = [sql.Identifier(key) for key in data.keys()]
        values = [sql.Literal(value) for value in data.values()]
        if len(data) == 1:
            query = sql.SQL(
                """
                    UPDATE
                        animes
                    SET
                        {columns} = {values}
                    WHERE
                        id={id}
                    RETURNING *
                """
            ).format(
                id=sql.Literal(anime_id),
                columns=sql.SQL(",").join(columns),
                values=sql.SQL(",").join(values),
            )
        else:
            query = sql.SQL(
                """
                    UPDATE
                        animes
                    SET
                        ({columns}) = ({values})
                    WHERE
                        id={id}
                    RETURNING *
                """
            ).format(
                id=sql.Literal(anime_id),
                columns=sql.SQL(",").join(columns),
                values=sql.SQL(",").join(values),
            )

        cur.execute(query)

        anime = cur.fetchone()

        commit_and_close(conn, cur)

        return dict(zip(Anime.anime_keys, anime))
    
    @staticmethod
    def delete_anime(anime_id):
        conn, cur = conn_cur()
        
        query = """
            DELETE FROM
                animes
            WHERE
                id = %s
            RETURNING *;
        """
        
        cur.execute(query, (anime_id,))
        
        deleted_anime = cur.fetchone()
        
        commit_and_close(conn, cur)
        
        return dict(zip(Anime.anime_keys, deleted_anime))
        
