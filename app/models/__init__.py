import psycopg2
from os import getenv

configs = {
    'host': getenv('HOST'),
    'database': getenv('DATABASE'),
    'user': getenv('USER'),
    'password': getenv('PASSWORD')
}


def conn_cur():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    return conn, cur


def commit_and_close(conn, cur):
    conn.commit()
    cur.close()
    conn.close()

def create_table():
    conn, cur = conn_cur()
    
    cur.execute("""CREATE TABLE IF NOT EXISTS animes(
        id BIGSERIAL PRIMARY KEY,
        anime VARCHAR(100) NOT NULL UNIQUE,
        released_date DATE NOT NULL,
        seasons INTEGER NOT NULL
        );
        """)
    
    commit_and_close(conn, cur)