class KeyError(Exception):
    keys = ['anime', 'released_date', 'seasons']
    
    def __init__(self, wrong_keys) -> None:
        self.message = {
            "Valid Keys" : KeyError.keys,
            "Wrong keys" : wrong_keys
        }
        super().__init__(self.message, wrong_keys)
    
    @classmethod
    def difference_keys(cls, list_b):
        diff = set(list_b) - set(cls.keys)
        return list(diff)