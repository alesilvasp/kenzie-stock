from flask import Blueprint, jsonify, request
import psycopg2
from app.controllers.anime_controller import anime_by_id, create_anime, delete_by_id, update_anime
from app.models.anime_model import Anime
from app.models import create_table
from app.exceptions.exception_keys import KeyError

bp_animes = Blueprint('animes', __name__, url_prefix='/api')


@bp_animes.route('/animes', methods=['GET', 'POST'])
def get_create():
    create_table()
    data = request.get_json()

    if request.method == 'GET':
        return jsonify(Anime.all_animes()), 200
    try:
        diff_keys = KeyError.difference_keys(list(data.keys()))

        if diff_keys:
            raise KeyError(diff_keys)
        return jsonify(create_anime(**data)), 201
    except psycopg2.Error as error:
        err = error.pgerror.split('=')[1][0:-2]
        return {'Error': f'{err}'}, 409
    except KeyError as err:
        return err.message, 422


@bp_animes.route('/animes/<int:id>')
def filter(id):
    create_table()
    try:
        return jsonify(anime_by_id(id)), 200
    except TypeError as err:
        return {'Not found': f'Anime id {id} not found in database'}, 404

@bp_animes.patch('/animes/<int:id>')
def update(id):
    create_table()
    data = request.get_json()
    try:
        diff_keys = KeyError.difference_keys(list(data.keys()))

        if diff_keys:
            raise KeyError(diff_keys)
        return jsonify(update_anime(id, data)), 200
    except TypeError as err:
        return {'Not found': f'Anime id {id} not found in database'}, 404
    except KeyError as err:
        return err.message, 422
    
@bp_animes.delete('/animes/<int:id>')
def delete(id):
    create_table()
    try:
        return jsonify(delete_by_id(id)), 204
    except TypeError as err:
        return {'Not found': f'Anime id {id} not found in database'}, 404