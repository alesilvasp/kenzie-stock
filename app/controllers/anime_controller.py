import psycopg2
from app.models import anime_model
from app.models.anime_model import Anime
from flask import jsonify, request

def create_anime(**kwargs):  
      
    anime = Anime(**kwargs)
    return anime.create_anime()

def anime_by_id(id):
    return Anime.get_by_id(id)

def update_anime(id, data):
    
    updated_anime = Anime.update_anime_by_id(id, data)

    return updated_anime

def delete_by_id(id):
    return Anime.delete_anime(id)
